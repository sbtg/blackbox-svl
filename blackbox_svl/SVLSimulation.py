#!/usr/bin/env python3
#
# Copyright (c) 2019-2021 LG Electronics, Inc.
#
# This software contains code licensed as described in LICENSE.
#

import os
from environs import Env
import lgsvl
import blackbox_svl
from blackbox_svl.utils import deserialize_log_file, set_lights_green
from blackbox_svl.Behaviors import *
from blackbox_svl.JsonParser import JsonParser
from random import randrange

class SVLSimulation:

    def __init__(self, config_file = None, apollo_host = None):
        
        self.sim = self.connect_to_simulator()

        # set config file path from config_file or set to default config file
        if config_file is not None:
            self.config_file_path = os.path.join(os.getcwd(), config_file)
        else:
            self.config_file_path = os.path.join(os.path.dirname(__file__), '..', 'examples', 'default_configuration.json')

        self.json_parser = JsonParser(self.config_file_path)
        self.map_id = self.json_parser.getMapId()
        self.apollo_host = apollo_host
        self.ego = None
        self.dummy_ego = None

    @staticmethod
    def connect_to_simulator():
        env = Env()
        sim = lgsvl.Simulator(env.str("LGSVL__SIMULATOR_HOST", lgsvl.wise.SimulatorSettings.simulator_host), env.int("LGSVL__SIMULATOR_PORT", lgsvl.wise.SimulatorSettings.simulator_port))
        return sim

    def reload_map(self):
        if self.map_id is not None:
            if self.sim.current_scene == self.map_id:    
                self.sim.reset()
            else:
                self.sim.load(self.map_id, 42)
        else:
            if self.sim.current_scene == lgsvl.wise.DefaultAssets.map_borregasave:
                self.sim.reset()
            else:
                self.sim.load(lgsvl.wise.DefaultAssets.map_borregasave, 42)

    def connect_to_apollo(self, ego):
        try:
            BRIDGE_HOST = self.apollo_host
            BRIDGE_PORT = 9090
            # Dreamview setup
            dv = lgsvl.dreamview.Connection(self.sim, ego, BRIDGE_HOST)
            dv.set_hd_map('San Francisco')
            dv.set_vehicle('Lincoln2017MKZ LGSVL')
            modules = [
                'Localization',
                'Transform',
                'Routing',
                'Prediction',
                'Planning',
                'Control'
            ]
            spawns = self.sim.get_spawn()
            destination = spawns[0].destinations[1]
            
            dv.enable_apollo(destination.position.x, destination.position.z, modules)


            # connect ego vehicle with our apollo bridge
            ego.connect_bridge(BRIDGE_HOST, BRIDGE_PORT)
        except:
            print("Connection to Apollo failed")

    def initialize_ego(self, ego_x_pos, ego_init_speed_m_s):
        spawns = self.sim.get_spawn()
        state = lgsvl.AgentState()
        state.transform = spawns[0]
        # print(spawns[0])
        # print(state.transform.position)
        forward = lgsvl.utils.transform_to_forward(spawns[0])
        right = lgsvl.utils.transform_to_right(spawns[0])
        
        state.transform.position += ego_x_pos * forward

        # Agents can be spawned with a velocity. Default is to spawn with 0 velocity
        state.velocity = ego_init_speed_m_s * forward

        ego = self.sim.add_agent("2e9095fa-c9b9-4f3f-8d7d-65fa2bb03921", lgsvl.AgentType.EGO, state)
        self.ego = ego

        self.connect_to_apollo(self.ego)
        
        # change the state so that dummy ego is behind the ego
        spawns = self.sim.get_spawn()
        dummy_state = lgsvl.AgentState()
        dummy_state.transform = spawns[0]
        
        dummy_state.transform.position += 5*right
        # spawn a dummy ego to collect the trace of the ego vehicle. 
        dummy_ego = self.sim.add_agent("34c713df-2fd3-43f7-8756-9576735013f1", lgsvl.AgentType.EGO, dummy_state)
        self.dummy_ego = dummy_ego

        # connect dummy ego vehicle with our logging bridge
        # dummy host and dummy port are used to connect to the simulator
        BRIDGE_HOST_LOG = os.environ.get("LGSVL__AUTOPILOT_0_HOST", "LOG")
        BRIDGE_PORT_LOG = int(os.environ.get("LGSVL__AUTOPILOT_0_PORT", 9090))
        self.dummy_ego.connect_bridge(BRIDGE_HOST_LOG,BRIDGE_PORT_LOG)

    def collect_trace(self):
        BRIDGE_HOST = os.environ.get("LGSVL__AUTOPILOT_0_HOST", "DISCONNECT")
        BRIDGE_PORT = int(os.environ.get("LGSVL__AUTOPILOT_0_PORT", 9090))
        self.dummy_ego.connect_bridge(BRIDGE_HOST,BRIDGE_PORT)
        # windows log file path
        # final_data = deserialize_log_file("C:\\Users\\dheer\\AppData\\LocalLow\\LGElectronics\\SVLSimulator\\simulation_log.json.gz")
        # linux log file path
        # get the path of the log file present in the user directory
        file_path = os.path.join(os.path.expanduser("~"), ".config/unity3d/LGElectronics/SVLSimulator/simulation_log.json.gz")
        final_data = deserialize_log_file(file_path)
        return final_data

    def run_test(self, ego_init_speed_m_s=5.0, ego_x_pos=5.0, pedestrian_speed=3.0, steptime=None, inp_signal = None, points = None, sim_duration=5, for_matlab=False, behavior= None):
        print("Running test")
        self.reload_map()

        self.initialize_ego(ego_x_pos, ego_init_speed_m_s)
        
        print("Current starting time = ", self.sim.current_time)
        print("Current starting frame = ", self.sim.current_frame)
        
        state = lgsvl.AgentState()
        behaviors  = Behaviors(self.json_parser, self.sim, steptime)
        if(behavior == "followInputSignal"):
            behaviors.FollowInpSignal(inp_signal)
        elif(behavior == "followPoints"):
            behaviors.FollowPoints(points)
            
        
        self.sim.run(time_limit=sim_duration)
        
        self.sim.stop()

        final_data = self.collect_trace()
     
        print("Current ending time = ", self.sim.current_time)
        print("Current ending frame = ", self.sim.current_frame)
        print("*************************     SIMULATION END     ********************")
        return final_data

if __name__ == "__main__":
    input("Press Enter to run the simulation")

    svl_sim = SVLSimulation("configuration.json", "10.218.101.181")
    print("here")
    trace = None
    trace = svl_sim.run_test(ego_init_speed_m_s = 10, ego_x_pos = 10, sim_duration=15)
    

